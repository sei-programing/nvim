nmap <Leader>s <Plug>(easymotion-s2)

" quick semi
nnoremap <Leader>; $a;<Esc> 
nmap <C-s> :w<CR>
nmap <C-q> :q<CR>

" split resize
nnoremap <Leader>> 10<C-w>>
nnoremap <Leader>< 10<C-w><

" shoter commands
cnoreabbrev tree NERDTreeToggle
cnoreabbrev blame Gblame
cnoreabbrev find NERDTreeFind
cnoreabbrev diff Gdiff

" NerdTree
nnoremap <Leader>n : NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

" plugs
nmap <Leader>p :Files<CR>
nmap <Leader>ag :Ag<CR>

" Buffer
map <Leader>ob :Buffer<cr>

"split
nnoremap <C-Left> :vsp<CR>
nnoremap <C-Down> :sp<CR>

"Tmux navegator
nnoremap <silent> <Leader><C-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <Leader><C-l> :TmuxNavigateRight<cr>
nnoremap <silent> <Leader><C-j> :TmuxNavigateDown<cr>
nnoremap <silent> <Leader><c-k> :TmuxNavigateUp<cr>
