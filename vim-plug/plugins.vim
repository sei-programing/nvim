" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  "autocmd VimEnter * PlugInstall
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')

    " Better Syntax Support
    Plug 'sheerun/vim-polyglot'

    "Status bar
    Plug 'maximbaz/lightline-ale'
    Plug 'itchyny/lightline.vim'
    
    "tree
    Plug 'preservim/nerdtree'

    " Gruvbox theme
    Plug 'morhetz/gruvbox'   
    Plug 'godlygeek/csapprox'
  	Plug 'powerline/powerline'    
    Plug 'easymotion/vim-easymotion'

    "autocomplete
    Plug 'sirver/ultisnips'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}

    "typing
    Plug 'jiangmiao/auto-pairs'
    Plug 'alvan/vim-closetag'
    Plug 'tpope/vim-surround'
    
    "tmux
    Plug 'christoomey/vim-tmux-navigator'

    "IDE
    Plug 'editorconfig/editorconfig-vim'
    Plug 'junegunn/fzf'
    Plug 'junegunn/fzf.vim'
    Plug 'terryma/vim-multiple-cursors'
    Plug 'mhinz/vim-signify'
    Plug 'yggdroot/indentline'
    Plug 'scrooloose/nerdcommenter'
    
    "git
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-repeat'
    call plug#end()


